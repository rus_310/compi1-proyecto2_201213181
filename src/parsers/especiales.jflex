package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos5
%class lexico5
%cup
%public
%unicode
%line
%column
%char
%ignorecase


identificador= [a-zA-Z]+ [0-9]*

valor= [0-9]+

idovalor= ([a-zA-Z]|[0-9])+

logico= ("<"|">")

comentario= ["$"]({identificador}|" "|"-"|"_")*["$"]

%%

/*PALABRAS RESERVADAS*/

    "new"                   {return new Symbol(tabla_simbolos5.nuevo, yychar,yyline); }
    "entero"                {return new Symbol(tabla_simbolos5.entero, yychar,yyline); }
    "if"                    {return new Symbol(tabla_simbolos5.cicloif, yychar,yyline); }
    "while"                 {return new Symbol(tabla_simbolos5.ciclowhile, yychar,yyline); }
    "for"                   {return new Symbol(tabla_simbolos5.ciclofor, yychar,yyline); }
    "return"                {return new Symbol(tabla_simbolos5.retornar, yychar,yyline); }
    "<funcion>"             {return new Symbol(tabla_simbolos5.abrefuncion, yychar,yyline); }
    "</funcion>"            {return new Symbol(tabla_simbolos5.cierrafuncion, yychar,yyline); }
    "<restriccion>"         {return new Symbol(tabla_simbolos5.abrerest, yychar,yyline); }
    "</restriccion>"        {return new Symbol(tabla_simbolos5.cierrarest, yychar,yyline); }
    "<codigo>"              {return new Symbol(tabla_simbolos5.abrecodigo, yychar,yyline); }
    "</codigo>"             {return new Symbol(tabla_simbolos5.cierracodigo, yychar,yyline); }

    /* SEPARADOR */
    "["                     {return new Symbol(tabla_simbolos5.llavea, yychar,yyline); }
    "]"                     {return new Symbol(tabla_simbolos5.llavec, yychar,yyline); }
    "{"                     {return new Symbol(tabla_simbolos5.corchetea, yychar,yyline); }
    "}"                     {return new Symbol(tabla_simbolos5.corchetec, yychar,yyline); }
    ","                     {return new Symbol(tabla_simbolos5.coma, yychar,yyline); }
    "("                     {return new Symbol(tabla_simbolos5.abrep, yychar,yyline); }
    ")"                     {return new Symbol(tabla_simbolos5.cierrap, yychar,yyline); }
    ";"                     {return new Symbol(tabla_simbolos5.puntocoma, yychar,yyline); }
    "=="                    {return new Symbol(tabla_simbolos5.asignacion, yychar,yyline); }
    "="                     {return new Symbol(tabla_simbolos5.igual, yychar,yyline); }
    "+"                     {return new Symbol(tabla_simbolos5.suma, yychar,yyline); }
    "-"                     {return new Symbol(tabla_simbolos5.resta, yychar,yyline); }
    "*"                     {return new Symbol(tabla_simbolos5.multiplicacion, yychar,yyline); }
    "/"                     {return new Symbol(tabla_simbolos5.division, yychar,yyline); }

    {identificador}         {return new Symbol(tabla_simbolos5.identificador, yychar,yyline, new String(yytext())); }
    {valor}                 {return new Symbol(tabla_simbolos5.valor, yychar,yyline, new String(yytext())); }
    {logico}                {return new Symbol(tabla_simbolos5.logico, yychar,yyline, new String(yytext())); }
    {idovalor}              {return new Symbol(tabla_simbolos5.idovalor, yychar,yyline, new String(yytext())); }
    {comentario}              {return new Symbol(tabla_simbolos5.comentario, yychar,yyline, new String(yytext())); }
    
/* BLANCOS */
[ \t\r\f]+ { /* Se ignoran */}
[\n]+      {return new Symbol(tabla_simbolos5.salto, yychar,yyline);}
/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos5.errorlex, yycolumn,yyline, new String(yytext())); }