package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos4
%class lexico4
%cup
%public
%unicode
%line
%column
%char
%ignorecase


identificador= [a-zA-Z]+ [0-9]*

valor= [0-9]+

idovalor= ({identificador}|{valor})*

logico= ("<"|">")

comentario= ["$"]({identificador}|" "|"-"|"_")*["$"]
%%

/*PALABRAS RESERVADAS*/

    "new"                   {return new Symbol(tabla_simbolos4.nuevo, yychar,yyline); }
    "entero"                {return new Symbol(tabla_simbolos4.entero, yychar,yyline); }
    "if"                    {return new Symbol(tabla_simbolos4.cicloif, yychar,yyline); }
    "while"                 {return new Symbol(tabla_simbolos4.ciclowhile, yychar,yyline); }
    "for"                   {return new Symbol(tabla_simbolos4.ciclofor, yychar,yyline); }
    "return"                {return new Symbol(tabla_simbolos4.retornar, yychar,yyline); }

    /* SEPARADOR */
    "["                     {return new Symbol(tabla_simbolos4.llavea, yychar,yyline); }
    "]"                     {return new Symbol(tabla_simbolos4.llavec, yychar,yyline); }
    "{"                     {return new Symbol(tabla_simbolos4.corchetea, yychar,yyline); }
    "}"                     {return new Symbol(tabla_simbolos4.corchetec, yychar,yyline); }
    ","                     {return new Symbol(tabla_simbolos4.coma, yychar,yyline); }
    "("                     {return new Symbol(tabla_simbolos4.abrep, yychar,yyline); }
    ")"                     {return new Symbol(tabla_simbolos4.cierrap, yychar,yyline); }
    ";"                     {return new Symbol(tabla_simbolos4.puntocoma, yychar,yyline); }
    "=="                    {return new Symbol(tabla_simbolos4.asignacion, yychar,yyline); }
    "="                     {return new Symbol(tabla_simbolos4.igual, yychar,yyline); }
    "+"                     {return new Symbol(tabla_simbolos4.suma, yychar,yyline); }
    "-"                     {return new Symbol(tabla_simbolos4.resta, yychar,yyline); }
    "*"                     {return new Symbol(tabla_simbolos4.multiplicacion, yychar,yyline); }
    "/"                     {return new Symbol(tabla_simbolos4.division, yychar,yyline); }
 

    {identificador}         {return new Symbol(tabla_simbolos4.identificador, yychar,yyline, new String(yytext())); }
    {valor}                 {return new Symbol(tabla_simbolos4.valor, yychar,yyline, new String(yytext())); }
    {logico}                {return new Symbol(tabla_simbolos4.logico, yychar,yyline, new String(yytext())); }
    {idovalor}              {return new Symbol(tabla_simbolos4.idovalor, yychar,yyline, new String(yytext())); }
    {comentario}            {return new Symbol(tabla_simbolos4.comentario, yychar,yyline, new String(yytext())); }
    
/* BLANCOS */
[ \t\r\f]+ { /* Se ignoran */}

[\n]+      {return new Symbol (tabla_simbolos4.salto, yychar,yyline);}

/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos4.errorlex, yycolumn,yyline, new String(yytext())); }