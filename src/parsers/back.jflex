package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos3
%class lexico3
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numero =[0-9]+ "."? [0-9]*
palabra= [a-zA-Z]+




%%

    

    /* SEPARADOR */
    "{"                     {return new Symbol(tabla_simbolos3.corchetea, yychar,yyline); }
    "}"                     {return new Symbol(tabla_simbolos3.corchetec, yychar,yyline); }
    ":"                     {return new Symbol(tabla_simbolos3.dospuntos, yychar,yyline); }
    ","                     {return new Symbol(tabla_simbolos3.coma, yychar,yyline); }
    "("                     {return new Symbol(tabla_simbolos3.abrep, yychar,yyline); }
    ")"                     {return new Symbol(tabla_simbolos3.cierrap, yychar,yyline); }


    {palabra}             {return new Symbol(tabla_simbolos3.palabra, yychar,yyline, new String(yytext())); }
    {numero}              {return new Symbol(tabla_simbolos3.numero, yychar,yyline, new String(yytext())); }
    
/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos3.errorlex, yycolumn,yyline, new String(yytext())); }