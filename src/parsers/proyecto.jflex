package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos1
%class lexico1
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numero =[0-9]+ "."? [0-9]*
palabra =[a-zA-Z]+

oracion = {palabra}({palabra}|{numero}| ":" | "_" | "\\" |"["|"]"| "." | ",")*

sstring = [\"]{oracion}[\"]

dia = [0-9]+["/"][0-9]+["/"][0-9]+


%%

    /* PROPIEDADES */
    "path"                  {return new Symbol(tabla_simbolos1.path, yychar,yyline); }
    "nombre"              {return new Symbol(tabla_simbolos1.nombre, yychar,yyline); }
    "fecha"              {return new Symbol(tabla_simbolos1.fecha, yychar,yyline); }

    /* SEPARADOR */
    "["                     {return new Symbol(tabla_simbolos1.llavea, yychar,yyline); }
    "]"                     {return new Symbol(tabla_simbolos1.llavec, yychar,yyline); }
    ":"                     {return new Symbol(tabla_simbolos1.dospuntos, yychar,yyline); }
    ";"                     {return new Symbol(tabla_simbolos1.puntocoma, yychar,yyline); }

    {oracion}             {return new Symbol(tabla_simbolos1.oracion, yychar,yyline, new String(yytext())); }
    {sstring}             {return new Symbol(tabla_simbolos1.sstring, yychar, yyline, new String(yytext())); }
    {numero}              {return new Symbol(tabla_simbolos1.numero, yychar, yyline, new String(yytext())); }
    {dia}              {return new Symbol(tabla_simbolos1.dia, yychar, yyline, new String(yytext())); }
   
    
/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos1.errorlex, yycolumn,yyline, new String(yytext())); }