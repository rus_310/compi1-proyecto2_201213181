package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos2
%class lexico2
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numero =[0-9]+ "."? [0-9]*
id =[A-Z]+
palabra= [a-zA-Z]+
oracion = ({palabra}|{numero}| "_" |" "| ".")+




%%

    /* PROPIEDADES */
    "columna"                  {return new Symbol(tabla_simbolos2.columna, yychar,yyline); }
    

    /* SEPARADOR */
    "{"                     {return new Symbol(tabla_simbolos2.corchetea, yychar,yyline); }
    "}"                     {return new Symbol(tabla_simbolos2.corchetec, yychar,yyline); }
    ":"                     {return new Symbol(tabla_simbolos2.dospuntos, yychar,yyline); }
    "["                     {return new Symbol(tabla_simbolos2.llavea, yychar,yyline); }
    "]"                     {return new Symbol(tabla_simbolos2.llavec, yychar,yyline); }
    ","                     {return new Symbol(tabla_simbolos2.coma, yychar,yyline); }


    {oracion}               {return new Symbol(tabla_simbolos2.oracion, yychar,yyline, new String(yytext())); }
    {id}                    {return new Symbol(tabla_simbolos2.id, yychar, yyline, new String(yytext())); }
   
    
/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos2.errorlex, yycolumn,yyline, new String(yytext())); }